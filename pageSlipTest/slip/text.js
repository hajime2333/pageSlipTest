
function getPageAndSize() {
    console.log('123');
    //判断是否有table，有就删除table，ai生成
    function hasTableTag() {
        const tables = document.querySelectorAll('table');
        return tables.length > 0;
    }
    // 调用函数进行判断，ai
    if (hasTableTag()) {
        console.log('页面中有 <table> 标签');
        //删除table标签ai
        const tables2 = document.body.getElementsByTagName('table');
        if (tables2.length > 0) {
            const tableToRemove = tables2[0];
            document.body.removeChild(tableToRemove);
        }
    } else {
        console.log('页面中没有 <table> 标签');
    }
    //获取输入的数值，半抄半写
    const inputField1 = document.getElementById("iptPage");
    const inputValue1 = inputField1.value;
    const inputField2 = document.getElementById("iptPageSize");
    const inputValue2 = inputField2.value;

    const page = inputValue1;
    const page_size = inputValue2;

const url = `http://127.0.0.1:3000/pageSlip?page=${page}&page_size=${page_size}`;
//dom操作，以下全是ai生成
fetch(url)
.then(response => response.json())
.then(responseData => {
      const table = document.createElement('table');
      // 创建表头
      const thead = document.createElement('thead');
      const headRow = document.createElement('tr');
      const headers = ['ID', 'Name', 'Email'];
      headers.forEach(header => {
          const th = document.createElement('th');
          th.textContent = header;
          headRow.appendChild(th);
      });
      thead.appendChild(headRow);
      table.appendChild(thead);

      // 创建表体
      const tbody = document.createElement('tbody');
      responseData.data.forEach(item => {
          const row = document.createElement('tr');
          const idCell = document.createElement('td');
          idCell.textContent = item.id;
          const nameCell = document.createElement('td');
          nameCell.textContent = item.name;
          const emailCell = document.createElement('td');
          emailCell.textContent = item.email;
          row.appendChild(idCell);
          row.appendChild(nameCell);
          row.appendChild(emailCell);
          tbody.appendChild(row);
      });
      table.appendChild(tbody);

      document.body.appendChild(table);
  });
};